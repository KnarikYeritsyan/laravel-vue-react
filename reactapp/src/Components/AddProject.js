import React, { Component } from 'react';
import uuid from 'uuid'
import ProjectItem from "./ProjectItem";

class AddProject extends Component {
    constructor(){
        super();
        this.state = {
            newProject:{}
        }
    }
    static defaultProps = {
        categories:['Web Design','Web Development','Mobile Development']
    };
    handleSubmit(e){
        if (this.refs.title.value===''){
            alert("Title is required");
        }else {
            this.setState({newProject:{
                id:uuid.v4(),
                title:this.refs.title.value,
                    category:this.refs.category.value
                }},function(){
                // console.log(this.state);
                this.props.addProject(this.state.newProject);
            });
        }
        e.preventDefault();
        // console.log(this.refs.title.value);
    }
    render() {
        let categoryOptions = this.props.categories.map(category=>{
            return <option key={category} value={category}>{category}</option>
        });
        let projectItems;
        if(this.props.projects){
            projectItems = this.props.projects.map(project=>{
                // console.log(project);
                return (
                    <ProjectItem key={project.title} project={project}/>
                );
            });
        }
        // console.log(this.props);
        return (
            <div>
                <h3>Add Project</h3>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <label>Title</label>
                        <input type="text" ref="title"/>
                    </div>
                    <div>
                        <label>Category</label>
                        <select ref="category">
                            {categoryOptions}
                        </select>
                    </div>
                    <div>
                        <input type="submit" value="Submit"/>
                    </div>
                </form>
            </div>
        );
    }
}

export default AddProject;
