// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vueResource from 'vue-resource'
import router from './router'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate);
Vue.use(vueResource);

Vue.config.productionTip = false;
Vue.prototype.$ApiHost = 'http://laravel.local'

/* eslint-disable no-new */
new Vue({
    beforeCreate: function() {
        console.log(this.$ApiHost)
    },
  // el: '#app',
  router,
  components: { App },
  template: '<App/>'
}).$mount('#app');
